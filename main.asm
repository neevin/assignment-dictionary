; В main.asm определите функцию _start, которая:
; 
; Читает строку размером не более 255 символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу.
; Иначе выдает сообщение об ошибке.
; 
; Не забудьте, что сообщения об ошибках нужно выводить в stderr.

%include 'words.inc'

%define ENDL 0xA
%define BUFFER_SIZE 256
%define SIZE 8

section .data
buffer: times BUFFER_SIZE db 0 ; Буффер для хранения строки

input_key: db 'Введите ключ для поиска:', ENDL, 0
long_key: db 'Ключ слишком длинный', ENDL, 0
success: db 'Значение ключа найдено:', ENDL, 0
no_key: db 'Этого ключа в словаре нет!', ENDL, 0


global _start

extern read_word
extern print_string
extern print_newline
extern exit
extern find_word

section .text

_start: 
        mov rdi, input_key
        call print_string

        mov rdi, buffer
        mov rsi, BUFFER_SIZE
        call read_word

        cmp rax, 0 ; Проверить, если в rax 0, то прочитать не удалось
        je .print_long_key

        push rdx ; Сохраняем длину ключа, чтобы потом её скипнуть
        mov rdi, buffer ; Ложим ключ
        mov rsi, top ; Ложим конец словаря
        call find_word

        cmp rax, 0
        je .print_no_key

        pop rdx
        add rax, SIZE
        add rax, rdx
        inc rax
        push rax ; Сохраняем адрес начала значения

        mov rdi, success
        call print_string
        pop rdi
        call print_string
        call print_newline
        jmp .end

    .print_no_key:
        mov rdi, no_key
        call print_string
        jmp .end
    .print_long_key:
        mov rdi, long_key
        call print_string
    .end:
        mov rdi, 0
        call exit
