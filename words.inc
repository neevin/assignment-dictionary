; Файл words.inc должен хранить слова, определённые с помощью макроса colon.
; Включите этот файл в main.asm.

%include 'colon.inc'


colon 'd', d
db 'forth string', 0
colon 'c', c
db 'third string', 0
colon 'b', b
db 'second string', 0
colon 'a', a
db 'first srting', 0
